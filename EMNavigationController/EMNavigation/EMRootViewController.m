//
//  RootViewController.m
//  EMultiNavigation
//
//  Created by EasonWang on 13-11-5.
//  Copyright (c) 2013年 EasonWang. All rights reserved.
//


#define SELECTED_VIEW_CONTROLLER_TAG 98456345

#import "EMRootViewController.h"
#import "EMTabViewController.h"
#import "EMNavigationController.h"

@interface EMRootViewController ()

@end

@implementation EMRootViewController

static EMRootViewController *rootVC;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    rootVC = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return rootVC;
}

+(EMRootViewController *)instance
{
    return rootVC;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    _arrayViewControllers = [EMTabViewController getTabViewControllers];
    
    tabbar = [[TabBarView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height-49, 320, 49) viewControllers:_arrayViewControllers backColor:nil Alpha:0.8f setDelegate:self];
    
    [self.view addSubview:tabbar];
    
    [self touchBtnAtIndex:0];   // setting the default viewController
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


-(void)touchBtnAtIndex:(NSInteger)index
{
    UIView* currentView = [self.view viewWithTag:SELECTED_VIEW_CONTROLLER_TAG];
    [currentView removeFromSuperview];
    
    NSDictionary* data = _arrayViewControllers[index];
    
    UIViewController *viewController = data[@"viewController"];
    viewController.view.tag = SELECTED_VIEW_CONTROLLER_TAG;
    
//    viewController.view.frame = CGRectMake(0,0,self.view.frame.size.width, self.view.frame.size.height);
    [self.view insertSubview:viewController.view belowSubview:tabbar];
}
@end



#pragma mark -

@implementation TabBarView

static TabBarView *tabbar;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        //        self.backgroundColor = [UIColor redColor];
        // Initialization code
    }
    return self;
}


-(TabBarView *)initWithFrame:(CGRect)frame viewControllers:(NSArray *)controllers backColor:(UIColor*)color Alpha:(float)alpha setDelegate:(id<TabBarViewDelegate>)delegate
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = color;
        self.alpha = alpha;
        self.tDelegate = delegate;
        arrayButton = [[NSMutableArray alloc]initWithCapacity:0];
        int VCNumber = controllers.count;
        float butWidth = 320/controllers.count;
        for (int i = 0; i<VCNumber; i++) {
            NSDictionary *dicVC = controllers[i];
            UIButton *button = [[UIButton alloc]initWithFrame:CGRectMake(butWidth*i, 0, butWidth, frame.size.height)];
            [button setTag:i];
            [button setImage:[UIImage imageNamed:dicVC[@"imageDefault"]] forState:UIControlStateNormal];
            [button setImage:[UIImage imageNamed:dicVC[@"imageSelected"]] forState:UIControlStateSelected];
            
            [button addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
            [self addSubview:button];
            [arrayButton addObject:button];
            i==0?[button setSelected:YES]:[button setSelected:NO];
        }
    }
    return self;
}

-(void)buttonClick:(id)sender
{
    UIButton *button = (UIButton *)sender;
    for (UIButton *but in arrayButton) {
        if (button.tag == but.tag) {
            but.selected = YES;
            [self.tDelegate touchBtnAtIndex:button.tag];
        }else{
            but.selected = NO;
        }
    }
}

@end
