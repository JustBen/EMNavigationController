//
//  EMViewController.h
//  EMultiNavigation
//
//  Created by EasonWang on 13-11-24.
//  Copyright (c) 2013年 EasonWang. All rights reserved.
//


/**
 EMViewController继承自UIViewController
 实现，iOS6、iOS7 的适配
 实现，屏幕尺寸的适配（3.5寸屏和4寸屏）
 实现，自定义的 NavigationBar，可以添加title,可修改返回按钮和右侧的功能按钮，可自定义添加按钮
 */


// RootNavigationController
#define NavigationController [[EMRootViewController instance] navigationController]


//屏幕尺寸适配相关
#define IPHONE5 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size) : NO)
//版本判断
#define VERSION ([[UIDevice currentDevice].systemVersion doubleValue]>=7?YES:NO)

/*
 * 设置页面 bounds
 */
#define setViewBounds(_obj){  \
CGRect temp = _obj.view.bounds; \
if(VERSION){ \
temp.origin.y = -20;\
} \
_obj.view.bounds = temp;\
}


#import <UIKit/UIKit.h>
#import "EMRootViewController.h"
#import "LoadingView.h"
@interface EMViewController : UIViewController
{
    UIButton *_backButton;
    UIView *_NavView;
    UILabel *_titleLabel;
    UIImageView *_navBackGroundImageView;
}

/**
 导航背景颜色 backGroundColor
 */
@property (nonatomic, strong) UIColor *navBackGroundColor;

/**
 导航背景图片 backGroundImage
 */
@property (nonatomic, strong) UIImage *navBackGroundImage;

/**
 导航title
 */
@property (nonatomic, strong) NSString *navTitle;

/**
 导航title的字体颜色
 */
@property (nonatomic, strong) UIColor *navTitleColor;

/**
 返回按钮title
 */
@property (nonatomic, strong) NSString *backBtnTitle;

/**
 返回按钮image
 */
@property (nonatomic, strong) UIImage *backBtnImage;

/**
 活动指示器（LoadingView）
 */
@property (nonatomic, strong) LoadingView *loadingView;

#pragma mark - navigationBar items method

/**
 设置leftItem(title)
 @param target 目标类对象，应设置为self，及为本类对象
 @param action 目标方法，设置与item相关联的selector
 @param title item标题，item显示的标题
 */
- (void)setLeftItemWithTarget:(id)target
                       action:(SEL)action
                        title:(NSString *)title;
/**
 设置leftItem(image)
 @param target 目标类对象，应设置为self，及为本类对象
 @param action 目标方法，设置与item相关联的selector
 @param image image图片，及用图片设置LeftItem
 */
- (void)setLeftItemWithTarget:(id)target
                       action:(SEL)action
                        image:(UIImage *)image;

/**
 设置rightItem(title)
 @param target 目标类对象，应设置为self，及为本类对象
 @param action 目标方法，设置与item相关联的selector
 @param title item标题，item显示的标题
 */
- (void)setRightItemWithTarget:(id)target
                        action:(SEL)action
                         title:(NSString *)title;

/**
 设置rightItem(image)
 @param target 目标类对象，应设置为self，及为本类对象
 @param action 目标方法，设置与item相关联的selector
 @param image image图片，及用图片设置LeftItem
 */
- (void)setRightItemWithTarget:(id)target
                        action:(SEL)action
                         image:(UIImage *)image;

@end





